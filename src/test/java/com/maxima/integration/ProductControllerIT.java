package com.maxima.integration;

import com.maxima.entity.Product;
import com.maxima.repository.ProductRepository;
import com.maxima.util.ProductCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private ProductRepository productRepositoryMock;

    @BeforeEach
    public void setUp() {

        BDDMockito.when(productRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(ProductCreator.createProductValid()));

        BDDMockito.when(productRepositoryMock.save(ProductCreator.createProductToBeSaved()))
                .thenReturn(ProductCreator.createProductValid());

        BDDMockito.doNothing().when(productRepositoryMock).delete(ArgumentMatchers.any(Product.class));

        BDDMockito.when(productRepositoryMock.save(ProductCreator.createProductValid()))
                .thenReturn(ProductCreator.createProductUpdate());
    }


    @Test
    @DisplayName("findById returns an product when successful")
    public void findById_ReturnListOfProductsInsidePageObject_WhenSuccessful() {
        Long expectedId = ProductCreator.createProductValid().getId();

        Product product = testRestTemplate.getForObject("/product/1", Product.class);

        Assertions.assertThat(product).isNotNull();

        Assertions.assertThat(product.getId()).isNotNull();

        Assertions.assertThat(product.getId()).isEqualTo(expectedId);
    }

    @Test
    @DisplayName("save creates an product when successful")
    public void save_CreatesProduct_WhenSuccessful() {
        Long expectedId = ProductCreator.createProductValid().getId();

        Product productToBeSaved = ProductCreator.createProductToBeSaved();

        ResponseEntity<Object> exchange = testRestTemplate.exchange("/product", HttpMethod.POST,
                createJsonHttpEntity(productToBeSaved), Object.class);

        Assertions.assertThat(exchange).isNotNull();

    }

    @Test
    @DisplayName("delete removes the product when successful")
    public void delete_RemovesProduct_WhenSuccessful() {

        ResponseEntity<Void> responseEntity = testRestTemplate.exchange("/product/1", HttpMethod.DELETE,
                null, Void.class);

        Assertions.assertThat(responseEntity).isNotNull();

        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        Assertions.assertThat(responseEntity.getBody()).isNull();
    }

    @Test
    @DisplayName("update save updated product when successful")
    public void update_SaveUpdatedProduct_WhenSuccessful() {
        Product validProduct = ProductCreator.createProductValid();

        ResponseEntity<Void> responseEntity = testRestTemplate.exchange("/product", HttpMethod.PUT,
                createJsonHttpEntity(validProduct), Void.class);

        Assertions.assertThat(responseEntity).isNotNull();

        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        Assertions.assertThat(responseEntity.getBody()).isNull();
    }

    private HttpEntity<Product> createJsonHttpEntity(Product product) {
        return new HttpEntity<>(product, createJsonHeader());
    }

    private HttpHeaders createJsonHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}
