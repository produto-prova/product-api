package com.maxima.integration;

import com.maxima.MaximaApplication;
import com.maxima.entity.Client;
import com.maxima.entity.Salesman;
import com.maxima.repository.ClientRepository;
import com.maxima.util.ClientCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClientControllerIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @MockBean
    private ClientRepository clientRepositoryMock;


    @BeforeEach
    public void setUp() {
        PageImpl<Client> clientPage = new PageImpl<>(List.of(ClientCreator.createClientValid()));
        BDDMockito.when(clientRepositoryMock.findAll(ArgumentMatchers.any(PageRequest.class)))
                .thenReturn(clientPage);

        BDDMockito.when(clientRepositoryMock.findById(ArgumentMatchers.anyLong()))
                .thenReturn(Optional.of(ClientCreator.createClientValid()));

        BDDMockito.when(clientRepositoryMock.save(ClientCreator.createClientValid()))
                .thenReturn(ClientCreator.createClientValid());

        BDDMockito.doNothing().when(clientRepositoryMock).delete(ArgumentMatchers.any(Client.class));

        BDDMockito.when(clientRepositoryMock.save(ClientCreator.createClientValid()))
                .thenReturn(ClientCreator.createClientUpdate());
    }

    @Test
    @DisplayName("findById returns an client when successful")
    public void findById_ReturnListOfClientsInsidePageObject_WhenSuccessful() {
        Long expectedId = ClientCreator.createClientValid().getId();

        Client client = testRestTemplate.getForObject("/client/1", Client.class);

        Assertions.assertThat(client).isNotNull();

        Assertions.assertThat(client.getId()).isNotNull();

        Assertions.assertThat(client.getId()).isEqualTo(expectedId);
    }


    @Test
    @DisplayName("save creates an client when successful")
    public void save_CreatesClient_WhenSuccessful() {
        Long expectedId = ClientCreator.createClientValid().getId();

        Client clientToBeSaved = ClientCreator.createClientToBeSaved();

        Salesman salesman = testRestTemplate.exchange("/salesman", HttpMethod.POST,
                createJsonHttpEntitySalesman(clientToBeSaved.getSalesman()), Salesman.class).getBody();

        clientToBeSaved.setSalesman(salesman);
        Client client = testRestTemplate.exchange("/client", HttpMethod.POST,
                createJsonHttpEntity(clientToBeSaved), Client.class).getBody();

        Assertions.assertThat(clientToBeSaved).isNotNull();

    }

    @Test
    @DisplayName("delete removes the client when successful")
    public void delete_RemovesClient_WhenSuccessful() {

        ResponseEntity<Void> responseEntity = testRestTemplate.exchange("/client/1", HttpMethod.DELETE,
                null, Void.class);

        Assertions.assertThat(responseEntity).isNotNull();

        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        Assertions.assertThat(responseEntity.getBody()).isNull();
    }
    
    @DisplayName("update save updated client when successful")
    public void update_SaveUpdatedClient_WhenSuccessful() {
        Client validClient = ClientCreator.createClientValid();

        ResponseEntity<Void> responseEntity = testRestTemplate.exchange("/client", HttpMethod.PUT,
                createJsonHttpEntity(validClient), Void.class);

        Assertions.assertThat(responseEntity).isNotNull();

        Assertions.assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        Assertions.assertThat(responseEntity.getBody()).isNull();
    }

    private HttpEntity<Client> createJsonHttpEntity(Client client) {
        return new HttpEntity<>(client, createJsonHeader());
    }

    private HttpEntity<Salesman> createJsonHttpEntitySalesman(Salesman salesman) {
        return new HttpEntity<>(salesman, createJsonHeader());
    }

    private HttpHeaders createJsonHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}


