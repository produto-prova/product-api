package com.maxima.util;

import com.maxima.entity.Client;
import com.maxima.request.ClientRequest;
import com.maxima.request.SalesmanRequest;
import com.maxima.service.MapperGeneric;

public class ClientCreator {

    public static Client createClientToBeSaved(){
        return MapperGeneric.parserToEntity(new ClientRequest("Cliente 1", "codigo 1",
                new SalesmanRequest(1L,"Vendedor 1", "codigo 1")), Client.class);

    }

    public static Client createClientUpdate(){
        return MapperGeneric.parserToEntity(new ClientRequest(1L, "Cliente 1", "codigo 1",
                new SalesmanRequest(1L, "Vendedor 1", "codigo 1")), Client.class);

    }

    public static Client createClientValid(){
        return MapperGeneric.parserToEntity(new ClientRequest(1L, "Cliente 1", "codigo 1",
                new SalesmanRequest(1L, "Vendedor 1", "codigo 1")), Client.class);
    }
}
