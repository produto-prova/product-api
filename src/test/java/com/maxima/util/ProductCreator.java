package com.maxima.util;

import com.maxima.entity.Product;
import com.maxima.request.ProductRequest;

import com.maxima.service.MapperGeneric;

public class ProductCreator {

    public static Product createProductToBeSaved(){
        return MapperGeneric.parserToEntity(new ProductRequest("Product 1", "codigo 1"), Product.class);

    }

    public static Product createProductUpdate(){
        return MapperGeneric.parserToEntity(new ProductRequest(1L, "Producte 1", "codigo 1"), Product.class);

    }

    public static Product createProductValid(){
        return MapperGeneric.parserToEntity(new ProductRequest(1L, "Producte 1", "codigo 1"), Product.class);
    }
}
