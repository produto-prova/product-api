package com.maxima.repository;

import com.maxima.entity.Salesman;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesmanRepository extends JpaRepository<Salesman, Long> {

    @Query(value = "select u from Salesman u where u.name like %:name%")
    Page<Salesman> listAllPageAndLikeName(@Param("name") String nome, Pageable pageable);
}
