package com.maxima.repository;

import com.maxima.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "select u from Product u where u.name like %:name%")
    Page<Product> listAllPageAndLikeName(@Param("name") String nome, Pageable pageable);
}
