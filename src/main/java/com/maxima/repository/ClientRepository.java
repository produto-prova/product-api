package com.maxima.repository;

import com.maxima.entity.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(value = "select u from Client u where u.name like %:name%")
    Page<Client> listAllPageAndLikeName(@Param("name") String nome, Pageable pageable);
}
