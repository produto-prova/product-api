package com.maxima.controller;

import com.maxima.request.ProductRequest;
import com.maxima.response.ProductResponse;
import com.maxima.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/product")
public class ProductController {

    private final ProductService productService;

    @GetMapping
    @Operation(summary = "List all product paginated")
    public ResponseEntity<Page<ProductResponse>> listPage(@RequestParam(value = "name", required = false, defaultValue = "") String name, Pageable pageable) {
        return ResponseEntity.ok(productService.findAllPageable(pageable, name));
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "List product by id")
    public ResponseEntity<ProductResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(productService.findByIdOrThrowRequestException(id));
    }

    @PostMapping
    @Operation(summary = "save product")
    @ApiResponse(responseCode = "201", description = "product is created", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ProductResponse.class))})
    public ResponseEntity<?> save(@Valid @RequestBody ProductRequest productRequest) {
        return ResponseEntity.ok(productService.save(productRequest));
    }

    @PutMapping
    @Operation(summary = "update product")
    @ApiResponse(responseCode = "201", description = "product is update", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ProductResponse.class))})
    public ResponseEntity<Void> replace(@Valid @RequestBody ProductRequest productRequest) {
        productService.replace(productRequest);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "delete product")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
