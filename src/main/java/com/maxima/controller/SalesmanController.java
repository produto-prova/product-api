package com.maxima.controller;

import com.maxima.request.SalesmanRequest;
import com.maxima.response.SalesmanResponse;
import com.maxima.service.SalesmanService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/salesman")
public class SalesmanController {

    private final SalesmanService salesmanService;

    @GetMapping
    @Operation(summary = "List all salesman paginated")
    public ResponseEntity<Page<SalesmanResponse>> listPage(@RequestParam(value = "name", required = false, defaultValue = "") String name, Pageable pageable) {
        return ResponseEntity.ok(salesmanService.findAllPageable(pageable, name));
    }

    @GetMapping("/list-all")
    @Operation(summary = "List all salesman")
    public ResponseEntity<List<SalesmanResponse>> list() {
        return ResponseEntity.ok(salesmanService.findAll());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "List salesman by id")
    public ResponseEntity<SalesmanResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(salesmanService.findByIdOrThrowRequestException(id));
    }

    @PostMapping
    @Operation(summary = "save salesman")
    @ApiResponse(responseCode = "201", description = "salesman is created", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = SalesmanResponse.class))})
    public ResponseEntity<?> save(@Valid @RequestBody SalesmanRequest salesmanRequest) {
        return ResponseEntity.ok(salesmanService.save(salesmanRequest));
    }

    @PutMapping
    @Operation(summary = "update salesman")
    @ApiResponse(responseCode = "201", description = "salesman is update", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = SalesmanResponse.class))})
    public ResponseEntity<Void> replace(@Valid @RequestBody SalesmanRequest salesmanRequest) {
        salesmanService.replace(salesmanRequest);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "delete salesman")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        salesmanService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
