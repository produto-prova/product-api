package com.maxima.controller;

import com.maxima.request.ClientRequest;
import com.maxima.response.ClientResponse;
import com.maxima.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/client")
public class ClientController {

    private final ClientService clientService;

    @GetMapping
    @Operation(summary = "List all client paginated")
    public ResponseEntity<Page<ClientResponse>> listPage(@RequestParam(value = "name", required = false, defaultValue = "") String name, Pageable pageable) {
        return ResponseEntity.ok(clientService.findAllPageable(pageable, name));
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "List client by id")
    public ResponseEntity<ClientResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(clientService.findByIdOrThrowRequestException(id));
    }

    @PostMapping
    @Operation(summary = "save client")
    @ApiResponse(responseCode = "201", description = "client is created", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ClientResponse.class))})
    public ResponseEntity<?> save(@Valid @RequestBody ClientRequest clientRequest) {
        return ResponseEntity.ok(clientService.save(clientRequest));
    }

    @PutMapping
    @Operation(summary = "update client")
    @ApiResponse(responseCode = "201", description = "client is update", content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ClientResponse.class))})
    public ResponseEntity<Void> replace(@Valid @RequestBody ClientRequest clientRequest) {
        clientService.replace(clientRequest);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "delete client")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        clientService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
