package com.maxima.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class BaseModel {

    private Long id;

    @Length(min = 3, max = 200, message = "O campo NOME deve ter entre 3 e 200 caracteres")
    @NotEmpty(message = "Codigo não pode está vazio")
    @NotNull(message = "Codigo não pode está com valor nulo")
    private String name;

    @Length(min = 1, max = 200, message = "O campo CODE deve ter entre 3 e 200 caracteres")
    @NotEmpty(message = "Nome não pode está vazio")
    @NotNull(message = "Nome não pode está com valor nulo")
    private String code;

    public BaseModel(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
