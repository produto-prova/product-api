package com.maxima.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxima.entity.BaseEntity;
import com.maxima.model.BaseModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MapperGeneric {

    public static <S extends BaseEntity, T extends BaseModel> T parserToModel(S source, Class<T> targetClass) {
        try {
            Optional.ofNullable(source).orElseThrow(() -> new Exception("object null"));

            return new ModelMapper().map(source, targetClass);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <S extends BaseModel, T extends BaseEntity> T parserToEntity(S source, Class<T> targetClass) {

        try {
            Optional.ofNullable(source).orElseThrow(() -> new Exception("object null"));

            return new ModelMapper().map(source, targetClass);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T extends BaseModel> Page<T> parserToModel(Page<?> lista, Type targetClass) {
        return new ModelMapper().map(lista, new TypeToken<Page<Type>>() {
        }.getType());
    }

    public static <T extends BaseModel> List<T> parserToModel(List<?> lista, Type targetClass) {
        return new ModelMapper().map(lista, new TypeToken<List<Type>>() {
        }.getType());
    }

    public static <S, T> List<T> parserToModel(List<S> source, Class<T> targetClass) {
        List<T> list = new ArrayList<>();
        for (S s : source) {
            list.add(new ModelMapper().map(s, targetClass));
        }
        return list;
    }

    public static <T> List<T> parserToModelList(String json, Class<T> clazz) {
        try {
            Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + clazz.getName() + ";");
            T[] objects = new ObjectMapper().readValue(json, arrayClass);
            return Arrays.asList(objects);
        } catch (Exception e) {
            return null;
        }
    }
}
