package com.maxima.service;

import com.maxima.entity.Product;
import com.maxima.exception.BadResquestException;
import com.maxima.repository.ProductRepository;
import com.maxima.request.ProductRequest;
import com.maxima.response.ProductResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Page<ProductResponse> findAllPageable(Pageable pageable, String name) {
        return MapperGeneric.parserToModel(productRepository.listAllPageAndLikeName(name, pageable), ProductResponse.class);
    }

    public ProductResponse findByIdOrThrowRequestException(Long id) {
        return MapperGeneric.parserToModel(productRepository.findById(id).orElseThrow(() ->
                new BadResquestException("Product not fould")), ProductResponse.class);
    }

    public Product save(ProductRequest productRequest) {
        return productRepository.save(MapperGeneric.parserToEntity(productRequest, Product.class));
    }

    public void delete(Long id) {
        productRepository.deleteById(id);
    }

    public void replace(ProductRequest productRequest) {
        Product productEntity = MapperGeneric.parserToEntity(productRequest, Product.class);
        productEntity.setId(this.findByIdOrThrowRequestException(productRequest.getId()).getId());
        productRepository.save(productEntity);
    }

}
