package com.maxima.service;

import com.maxima.entity.Salesman;
import com.maxima.exception.BadResquestException;
import com.maxima.repository.SalesmanRepository;
import com.maxima.request.SalesmanRequest;
import com.maxima.response.SalesmanResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SalesmanService {

    private final SalesmanRepository salesmanRepository;

    public Page<SalesmanResponse> findAllPageable(Pageable pageable, String name) {
        return MapperGeneric.parserToModel(salesmanRepository.listAllPageAndLikeName(name, pageable), SalesmanResponse.class);
    }

    public List<SalesmanResponse> findAll() {
        return MapperGeneric.parserToModel(salesmanRepository.findAll(), SalesmanResponse.class);
    }

    public SalesmanResponse findByIdOrThrowRequestException(Long id) {
        return MapperGeneric.parserToModel(salesmanRepository.findById(id).orElseThrow(() ->
                new BadResquestException("salesman not fould")), SalesmanResponse.class);
    }

    public Salesman save(SalesmanRequest salesmanRequest) {
        return salesmanRepository.save(MapperGeneric.parserToEntity(salesmanRequest, Salesman.class));
    }

    public void delete(Long id) {
        try {
            salesmanRepository.deleteById(id);
        } catch (Exception e) {
            throw new DataIntegrityViolationException("Voce não pode apagar pois está vinculado a um cliente");
        }
    }

    public void replace(SalesmanRequest salesmanRequest) {
        Salesman salesmanEntity = MapperGeneric.parserToEntity(salesmanRequest, Salesman.class);
        salesmanEntity.setId(this.findByIdOrThrowRequestException(salesmanRequest.getId()).getId());
        salesmanRepository.save(salesmanEntity);
    }

}
