package com.maxima.service;

import com.maxima.entity.Client;
import com.maxima.exception.BadResquestException;
import com.maxima.repository.ClientRepository;
import com.maxima.request.ClientRequest;
import com.maxima.response.ClientResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;

    public Page<ClientResponse> findAllPageable(Pageable pageable, String name) {
        return MapperGeneric.parserToModel(clientRepository.listAllPageAndLikeName(name, pageable), ClientResponse.class);
    }

    public ClientResponse findByIdOrThrowRequestException(Long id) {
        return MapperGeneric.parserToModel(clientRepository.findById(id).orElseThrow(() ->
                new BadResquestException("client not fould")), ClientResponse.class);
    }

    public Client save(ClientRequest clientRequest) {
        return clientRepository.save(MapperGeneric.parserToEntity(clientRequest, Client.class));
    }

    public void delete(Long id) {
        clientRepository.deleteById(id);
    }

    public void replace(ClientRequest clientRequest) {
        Client clientEntity = MapperGeneric.parserToEntity(clientRequest, Client.class);
        clientEntity.setId(this.findByIdOrThrowRequestException(clientRequest.getId()).getId());
        clientRepository.save(clientEntity);
    }

}
