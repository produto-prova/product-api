package com.maxima.response;

import com.maxima.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SalesmanResponse extends BaseModel {
}
