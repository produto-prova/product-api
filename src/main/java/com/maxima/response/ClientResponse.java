package com.maxima.response;

import com.maxima.model.BaseModel;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class ClientResponse extends BaseModel {

    private SalesmanResponse salesman;
}
