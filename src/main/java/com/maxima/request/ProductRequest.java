package com.maxima.request;

import com.maxima.model.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class ProductRequest extends BaseModel {

    public ProductRequest(Long id, String name, String code) {
        super(id, name, code);
    }

    public ProductRequest(String name, String code) {
        super(name, code);
    }
}
