package com.maxima.request;

import com.maxima.model.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class ClientRequest extends BaseModel {
    private SalesmanRequest salesman;

    public ClientRequest(Long id, String name, String code, SalesmanRequest salesman) {
        super(id, name, code);
        this.salesman = salesman;
    }

    public ClientRequest(String name, String code, SalesmanRequest salesman) {
        super(name, code);
        this.salesman = salesman;
    }

}
