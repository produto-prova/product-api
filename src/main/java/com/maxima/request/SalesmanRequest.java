package com.maxima.request;

import com.maxima.model.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class SalesmanRequest extends BaseModel {

    public SalesmanRequest(Long id, String name, String code) {
        super(id, name, code);
    }

    public SalesmanRequest(String name, String code) {
        super(name, code);
    }

}
