package com.maxima.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;


@Getter
@Setter
@Entity
@DynamicUpdate
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(name = "default_gen", sequenceName = "client_id_seq", allocationSize = 1)
public class Client extends BaseEntity {

    @ManyToOne(optional = false)
    @JoinColumn
    private Salesman salesman;


}
