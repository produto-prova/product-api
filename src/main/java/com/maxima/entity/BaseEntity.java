package com.maxima.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Getter
@Setter
@MappedSuperclass

public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 3270601256011818010L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_gen")
    @Access(AccessType.PROPERTY)
    protected Long id;


    @Column(unique = true, nullable = false)
    private String code;

    @Column(nullable = false)
    private String name;
}
